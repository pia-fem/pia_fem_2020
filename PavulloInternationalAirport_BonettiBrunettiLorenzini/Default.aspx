﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PavulloInternationalAirport_BonettiBrunettiLorenzini._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>INTERNATIONAL PAVULLO AIRPORT</h1>
        <p class="lead">L'aeroporto internazionale di Pavullo nel frignano. Sicuro, efficiente, potente.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Autenticati e usufruisci dei nostri servizi</h2>
            <p>
                Tutti gli strumenti messi a disposizione dal nostro software, disponibili con un click!
            </p>
            <p>
                <a class="btn btn-default" href="Autentication.aspx">Autenticati &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>

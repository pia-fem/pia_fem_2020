﻿<%@ Page Title="Autentication" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Autentication.aspx.cs" Inherits="PavulloInternationalAirport_BonettiBrunettiLorenzini.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <div class="container">
           <table>
   <tr>
      <td>Username:</td>
      <td><input id="txtUserName" type="text" runat="server" /></td>
      <td><ASP:RequiredFieldValidator ControlToValidate="txtUserName"
           Display="Static" ErrorMessage="*" runat="server" 
           ID="vUserName" /></td>
   </tr>
   <tr>
      <td>Password:</td>
      <td><input id="txtUserPass" type="password" runat="server" /></td>
      <td><ASP:RequiredFieldValidator ControlToValidate="txtUserPass"
          Display="Static" ErrorMessage="*" runat="server" 
          ID="vUserPass" />
      </td>
   </tr>
   <tr>
      <td>Persistent Cookie:</td>
      <td><ASP:CheckBox id="chkPersistCookie" runat="server" autopostback="false" /></td>
      <td></td>
   </tr>
</table>
  </div>
</asp:Content>